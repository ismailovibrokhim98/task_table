import { createSlice, nanoid } from '@reduxjs/toolkit'

const initialState = [
  { id: '1', title: 'React', content: 'I have heard good things' },
  { id: '2', title: 'Redux Toolkit', content: 'Redux toolkit new features' },
  { id: '3', title: 'Next js', content: 'Redux toolkit new features' },
]

const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    postAdded:{
      reducer(state, action) {
        state.push(action.payload)
      },
      prepare(title,content){
        return{
          payload:{
            id: nanoid(),
            title,
            content,
          }
        }
      }
    }
  }
})


export const { postAdded } = postsSlice.actions
export const SelectAllPosts = (state) => state.posts;

export default postsSlice.reducer
