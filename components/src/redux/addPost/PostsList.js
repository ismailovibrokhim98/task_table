import { useSelector } from "react-redux";
import React from 'react';
import { SelectAllPosts } from "./postsSlice";
import AddPostForm from "./AddPostForm";
import FetchData from "../fetchData/FetchData";
const PostsList = () => {
    const posts = useSelector(SelectAllPosts)
    return (
        <>
        <FetchData />
        -------------
        <AddPostForm />
            {
                 posts.map(post => (
                    <div key={post.id}  className="p-5 m-2 border bg-slate-300" >
                        <h1>{post.title }</h1>
                        <p>{post.content}</p>
                    </div>
                ))
            }
           
        </>
    );
}


export default PostsList;
