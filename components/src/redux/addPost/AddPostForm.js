import React, { useState } from 'react'
import { useDispatch} from 'react-redux'
import { postAdded } from './postsSlice'

const AddPostForm = () => {
  const dispatch = useDispatch()
  
  const [title, setTitle] = useState('')
  const [content, setContent] = useState('')

  const onTitleChange = (e) => setTitle(e.target.value)
  const onContentChange = (e) => setContent(e.target.value)

  const onSavePostClicked = () => {
    if (title && content) {
      dispatch(postAdded(title,content))

      setTitle('')
      setContent('')
    }
  }

  return (
    <div className="ml-10">
      <h2>Add a new Post</h2>
      <form className="flex flex-col w-96 ">
        <label htmlFor="postTitle">Post Title:</label>
        <input
          type="text"
          id="postTitle"
          name="postTitle"
          value={title}
          onChange={onTitleChange}
          className="border p-2"
        />

        <label htmlFor="postContent">Content:</label>
        <textarea
          id="postContent"
          name="postContent"
          value={content}
          onChange={onContentChange}
          className="border p-2"
        />

        <button
          type="button"
          onClick={onSavePostClicked}
          className="bg-blue-500 rounded-sm hover:bg-blue-700 mt-2 text-white py-2"
        >
          Save Post
        </button>
      </form>
    </div>
  )
}

export default AddPostForm
