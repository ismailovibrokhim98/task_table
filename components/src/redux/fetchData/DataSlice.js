import { createSlice } from "@reduxjs/toolkit";

export const siteDataSlice = createSlice({
    name:'siteData',
    initialState:{
        categories:[]
    },
    reducers:{
        saveCategories:(state,action) => {
            state.categories = action.payload
        }
    }
})

export const {saveCategories} = siteDataSlice.actions
export default siteDataSlice.reducer
