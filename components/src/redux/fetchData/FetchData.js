import React, { useEffect } from 'react'
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'
import { saveCategories } from './DataSlice'

const FetchData = () => {
  const dispatch = useDispatch()
  const categories = useSelector((state) => state.data.categories)

  useEffect(() => {
    async function getCategory() {
      try {
        const { data } = await axios({
          method: 'GET',
          url: 'https://market-index.herokuapp.com/api/home/category',
        })
        dispatch(saveCategories(data.data))
      } catch (e) {
        // return e
        console.log('eroor', e)
      }
    }
    getCategory()
  }, [dispatch])

  return (
    <div>
      <h1>Data fetched from Redux</h1>
      {categories.length && categories ? (
        categories.map((category) => {
          return (
            <ul key={category.id}>
              <li className="bg-red-400">{category.name_uz}</li>
            </ul>
          )
        })
      ) : (
        <p>Loading ...</p>
      )}
    </div>
  )
}

export default FetchData
