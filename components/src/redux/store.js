import { configureStore } from "@reduxjs/toolkit";
import postsReducer from "./addPost/postsSlice"
import dataReducer from "./fetchData/DataSlice"

export const store = configureStore({
    
    reducer:{
        posts: postsReducer,
        data: dataReducer
    }
})
