import React from 'react';

const AfterLogin = () => {
    return (
        <div className='w-full h-screen bg-green-600 flex items-center justify-center' >
            <h1 className='text-9xl text-white' >You Sucsessfully Logged</h1>
        </div>
    );
}

export default AfterLogin;
