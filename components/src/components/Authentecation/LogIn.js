import React, { useState } from 'react'
import { AiFillEyeInvisible } from 'react-icons/ai'
import { AiFillEye } from 'react-icons/ai'
import axios from 'axios'
import { Navigate } from 'react-router-dom'

const LOGIN_URL =
  'https://intex-shop-production.up.railway.app/api/admins/login'

const Token =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlNhcnZhciIsInJvbGVzIjoic3VwZXJfYWRtaW4iLCJpYXQiOjE2Njc2Mjc1MjEsImV4cCI6MTY2NzYzNDcyMX0.LP1LVcapEjVeCgR07hnLZNSEeLpbus5LdCjs671m5js'

const LogIn = () => {
  const [user, setUser] = useState('')
  const [password, setPassword] = useState('')
  const [ok, setOk] = useState(false)
  const [icon, setIcon] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault()
    try {
      const response = await axios.post(LOGIN_URL, {
        username: user,
        password: password,
      })
      localStorage.setItem('Token', Token)

      if (response.status === 201) {
        console.log('status')
        setOk(true)
      }

      setTimeout(() => {
        setOk(false)
      }, 3000)

      setUser('')
      setPassword('')
    } catch (err) {
      return err
    }
  }

  return (
    <div className="flex flex-col items-center justify-center  h-screen">
      <h1 className="text-2xl font-semibold pb-5 text-blue-600 ">
        Authentication
      </h1>
      <form
        className="flex flex-col w-96 border p-7 rounded-lg"
        onSubmit={handleSubmit}
      >
        <label htmlFor="name">Name</label>
        <input
          type="text"
          id="name"
          name="name"
          required
          placeholder="Enter your name"
          onChange={(e) => setUser(e.target.value)}
          value={user}
          className=" rounded-lg py-2 px-4 outline-none border border-gray-200"
        />
        <div className="relative flex flex-col">
          <label htmlFor="password" className="mt-2">
            Password
          </label>
          <input
            type={icon === true ? 'text' : 'password'}
            id="password"
            name="password"
            required
            placeholder="Enter your Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            className=" rounded-lg py-2 px-4 outline-none border border-gray-200"
          />
          <span onClick={() => setIcon(!icon)}>
            {icon === true ? (
              <AiFillEyeInvisible className="absolute right-3 top-10 w-6 h-6" />
            ) : (
              <AiFillEye className="absolute right-3 top-10 w-6 h-6" />
            )}
          </span>
        </div>
        <button
          className="bg-green-500 text-white rounded-sm mt-3 py-2 px-4"
          type="submit"
        >
          Log In
        </button>
      </form>
      {ok === true ? (
        <h1 className="text-2xl ease-in duration-700  animate-ping text-green-500">
          Sucsessfully
        </h1>
      ) : null}
    </div>
  )
}

export default LogIn
