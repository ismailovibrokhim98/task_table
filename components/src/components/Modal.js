import React, { Fragment, useState } from 'react'
import ModalComponent from './modal/ModalComponent'

const Modal = () => {
  const [showModal, setShowModal] = useState(false)
  const [showModal1, setShowModal1] = useState(false)
  const [showModal2, setShowModal2] = useState(false)
  return (
    <Fragment>
      <div className="p-10 ">
        <h1 className="text-4xl font-semibold text-gray-700 text-center mb-5">
          Creating custom modal in React JS
        </h1>

        <div className="flex items-center justify-center ">
          <button
            className="text-white bg-blue-700 hover:bg-blue-800 focus:outline-none font-medium text-sm rounded-lg px-5 py-2.5 text-center mr-5"
            onClick={() => setShowModal(true)}
          >
            Info modal
          </button>
          
          <button className="text-white bg-blue-700 hover:bg-blue-800 focus:outline-none font-medium text-sm rounded-lg px-5 py-2.5 text-center mr-5"onClick={() => setShowModal2(true)}>
            Registration
          </button>
        </div>
      </div>
      <ModalComponent isVisible={showModal}  onClose={() => setShowModal(false)}  >
        <div className="p-6">
          <h3 className="font-semibold text-xl text-gray-900 mb-5 ">
            Modal Title
          </h3>
          <p className="mb-5 text-gray-500">
            Using utilities to style elements on hover, focus, and more. Every
            utility class in Tailwind can be applied conditionally by adding a
            modifier to the beginning of the class name that describes the
            condition you want to target. For example, to apply the bg-sky-700
            class on hover, use the hover:bg-sky-700 class: Hover over this
            button to see the background color change
          </p>
          <button className="text-white bg-red-600 hover:bg-red-700 focus:outline-none font-medium text-sm rounded-lg px-5 py-2.5 text-center mr-5"onClick={() => setShowModal1(true)}>
            YouTube
          </button>
        </div>
      </ModalComponent>

      <ModalComponent isVisible={showModal1 }  onClose={() => setShowModal1(false)}>
        <div className="p-6">
          <h3 className="font-semibold text-xl text-gray-900 mb-5 ">
            Modal Title
          </h3>
          <p className="mb-5 text-red-700">
            Youtube
            dasssssssssssssiguiocfnhewqonirewhiqowefn
          </p>
        </div>
      </ModalComponent>

      <ModalComponent isVisible={showModal2}  onClose={() => setShowModal2(false)}>
        <div className="p-6">
          <h3 className="font-semibold text-xl text-gray-900 mb-5 ">
            Modal Title
          </h3>
          <p className="mb-5 text-blue-700">
            Twitter
            dasssssssssssssiguiocfnhewqonirewhiqowefn
          </p>
        </div>
      </ModalComponent>

    </Fragment>
  )
}

export default Modal
