import React from 'react'

const ModalComponent = ({ isVisible, onClose, children }) => {
  const handleClose = (e) => {
    if (e.target.id === 'wrapper') {
      onClose()
    }
  }

  if (!isVisible ) return null
  return (
    <div
      className=" fixed inset-0 bg-black bg-opacity-25 backdrop-blur-sm flex items-center justify-center "
      id="wrapper"
      onClick={handleClose}
    >
      <div className=" w-[600px]  ">
        <button className="text-xl text-white" onClick={handleClose}>
          X
        </button>
        <div className="bg-white p-2 ">{children}</div>
      </div>
    </div>
  )
}

export default ModalComponent
