import React, { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";
import UserList from "./components/UserList/UserList";
import FormInput from "./components/FormInput/FormInput";
import MButton from "./components/MButton/MButton";

const App = () => {
  // ! Userlarni saqlash uchun
  const [users, setUsers] = useState([]);
  // ! Userlarni api-dan olish uchun
  const getUsers = () => {
    axios
      .get("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/user")
      .then((res) => {
        setUsers(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // ! Component ishga tushganda userlarni olish
  useEffect(() => {
    getUsers();
  }, []);
  // ! Userni delete qilish uchun
  const deleteUser = (id) => {
    axios
      .delete(`https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/user/${id}`) // ! Hardoyim ham id bolib kelmaydi
      .then((res) => {
        getUsers();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // ! Formni dastlabki value-lari
  const intialValues = {
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
  };
  const [values, setValues] = useState(intialValues);

  // ! Formni submit qilish
  const onSubmit = (e) => {
    e.preventDefault();
    console.log("submit");
    const formData = {
      // username: values.username,
      // email: values.email,
      // password: values.password,
      // confirmPassword: values.confirmPassword,
      ...values, // ! Osonro yoli lekin initialValues bilan backenddagi value-lari bir xil bo'lishi kerak (username === username)
    };
    // ! axios.post() yangi user qo'shish uchun
    axios
      .post("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/user", formData)
      .catch((err) => console.log("err", err))
      .finally(() => {
        getUsers();
        setValues(intialValues);
      });
  };

  // ! HINT FOR FUTURE:
  const selectOptions = [
    { value: "", label: "Select..." },
    { value: "admin", label: "Admin" },
    { value: "user", label: "User" },
  ];

  const [selectedValue, setSelectedValue] = useState("");
  console.log("selectedValue", selectedValue);

  return (
    <>
      <div className="formWrapper">
        <h1>Create User</h1>
        <form className="form" onSubmit={onSubmit}>
          <FormInput
            required // ! required berilmasa validation bu polya uchun ishlamaydi
            pattern="^[a-zA-Z0-9]{3,11}$" // ! Validation uchun regex
            label="Username" // ! Label inputni tepasidagi text uchun
            name="username"
            value={values.username} // ! Value berilmasa handleChange ishlamaydi
            handleChange={
              (e) => setValues({ ...values, username: e.target.value }) // ! Input value-sini o'zgartirish uchun
            }
            errorMessage="Username is required, must be at least 3 characters"
          />
          <FormInput
            type="email"
            pattern={
              "^[a-zA-Z0-9.!#$%&'*+/=? ^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$"
            }
            errorMessage="Email is required"
            required
            label="Email"
            name="email"
            value={values.email}
            handleChange={(e) =>
              setValues({ ...values, email: e.target.value })
            }
          />
          <FormInput
            type="password"
            label="Password"
            name="password"
            required
            errorMessage="Password is required"
            pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
            value={values.password}
            handleChange={(e) =>
              setValues({ ...values, password: e.target.value })
            }
          />
          <FormInput
            required
            type="password"
            label="Confirm Password"
            name="confirmPassword"
            errorMessage="Confirm Password should match Password"
            pattern={values.password}
            value={values.confirmPassword}
            handleChange={(e) =>
              setValues({ ...values, confirmPassword: e.target.value })
            }
          />
          {/* // ! Custom Button */}
          <MButton
            BSize={"submit"}
            BStyle={{ marginBottom: "15px" }}
            type="submit"
          >
            Submit
          </MButton>
        </form>
      </div>
      {/* // ! Userlist */}
      <UserList users={users} deleteUser={deleteUser} /> 
      <select
        style={{ margin: "10px 60px", padding: "10px", minWidth: "200px" }}
        onChange={(e) => setSelectedValue(e.target.value)}
      >
        {/* // ! Backenddan axios orqali get qilib olgan dataniyam optionlarda aylansa bo'ladi */}
        {selectOptions.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </>
  );
};

export default App;
