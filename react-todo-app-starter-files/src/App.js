import React, { useState } from 'react'
import AppHeader from './components/AppHeader'
import Button from './components/Button'
import PageTitle from './components/PageTitle'
import styles from './styles/modules/app.module.scss'

import { toast, Toaster } from 'react-hot-toast'

function App() {
  const [data, setData] = useState([])

  function handleSubmit(e) {
    e.preventDefault()
    let { todoInput } = e.target.elements
    data.push(todoInput.value)
    window.localStorage.setItem('todo', JSON.stringify(data))
    console.log(todoInput.value)

    if (todoInput.value === '') {
      toast.error('unSuccessfull')
    } else {
      toast.success('Successfull done')
    }

    todoInput.value = null
  }

  return (
    <>
      <div className="container">
        <PageTitle>Todo List</PageTitle>
        <div className={styles.app__wrapper}>
          <AppHeader />

          <form onSubmit={handleSubmit}>
            <title>Name</title>
            <input
              type="text"
              style={{
                marginTop: '30px',
                marginRight: '20px',
                padding: '5px',
              }}
              // onChange={(e) => inputChange(e)}
              name="todoInput"
            />
            <Button type="submit" variant="primary">
              Add Assignment
            </Button>
          </form>
        </div>
      </div>
      <Toaster />
    </>
  )
}

export default App
