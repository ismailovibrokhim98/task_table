import TableC from './Components/Table_C/TableC'
import TableF from './Components/Table_F/TableF'

function App() {
  return (
    <div>
      <h1 className="text-4xl my-8 text-center font-semibold">
        Task done by{' '}
        <a className="text-blue-600 underline" href="mailto:ismailovibrokhim98@gmail.com">
          Ibrokhim
        </a>
      </h1>
      <TableF />
      {/* <TableC /> */}
    </div>
  )
}

export default App
