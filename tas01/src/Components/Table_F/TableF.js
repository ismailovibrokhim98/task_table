import React, { useEffect, useState } from 'react'
import axios from 'axios'
import sort from '../../Assets/images/sort.svg'

function TableF() {
  const [comments, setComments] = useState([])
  const [loading, setLoading] = useState(false)
  const [currentPage, setCurrentPage] = useState(1)
  const [limit, setLimit] = useState(10)
  const [openSort, setOpenSort] = useState(false)
  const [openSort1, setOpenSort1] = useState(false)
  const [queryName, setQueryName] = useState('')
  const [queryBody, setQueryBody] = useState('')
  const [pageNumberLimit, setPageNumberLimit] = useState(10)
  const [maxPageNumberLimit, setMaxPageNumberLimit] = useState(10)
  const [minPageNumberLimit, setMinPageNumberLimit] = useState(0)

  console.log(queryBody)

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)

      const res = await axios.get(
        `https://jsonplaceholder.typicode.com/comments`,
      )
      setComments(res.data)
      setLoading(false)
    }

    fetchData()
  }, [])

  const IndexLastPost = currentPage * limit
  const IndexFirstPost = IndexLastPost - limit
  const currentPost = comments.slice(IndexFirstPost, IndexLastPost)
  //  ==================  Pagination

  const pageNumbers = []

  for (let i = 1; i <= Math.ceil(comments.length / limit); i++) {
    pageNumbers.push(i)
  }

  const paginate = (pageNumbers) => setCurrentPage(pageNumbers)

  const handleNextBtn = () => {
    setCurrentPage(currentPage+1)

    if(setCurrentPage +1 > maxPageNumberLimit){
      setMaxPageNumberLimit(maxPageNumberLimit + pageNumberLimit);
      setMinPageNumberLimit(minPageNumberLimit + pageNumberLimit)
    }
  }


  const handlePrevBtn = () => {
    setCurrentPage(currentPage-1)

    if((currentPage-1)% pageNumberLimit == 0){
      setMaxPageNumberLimit(maxPageNumberLimit - pageNumberLimit);
      setMinPageNumberLimit(minPageNumberLimit - pageNumberLimit)
    }
  }

  return (
    <div className="  flex flex-col justify-center">
      <table className="w-[800px] ">
        <thead className=" bg-gray-900 ">
          <tr className="flex  rounded-t-3xl ">
            <th className=" flex justify-center min-w-[60px] py-3  text-md text-[#d6d6d6] font-medium">
              ID
            </th>
            <th className=" relative flex items-center min-w-[300px] py-3 px-2 text-md text-[#d6d6d6] font-medium">
              Name
              <img
                src={sort}
                alt="photo"
                className="ml-2 mt-1 cursor-pointer animate-pulse"
                width={20}
                onClick={() => setOpenSort(!openSort)}
              />
              <input
                type={'search'}
                onChange={(evt) => setQueryName(evt.target.value)}
                className={`${
                  openSort
                    ? 'absolute left-20 duration-500 transition-all'
                    : 'hidden'
                } outline-none border-none rounded-md bg-gray-200 py-1 px-2 text-black`}
              />
            </th>
            <th className=" flex items-start min-w-[300px] py-3 px-2 text-md text-[#d6d6d6] font-medium">
              Email
            </th>
            <th className="relative flex items-start min-w-[580px] py-3 px-2 text-md text-[#d6d6d6] font-medium">
              Body
              <img
                src={sort}
                alt="photo"
                className="ml-2 mt-1 cursor-pointer animate-pulse"
                width={20}
                onClick={() => setOpenSort1(!openSort1)}
              />
              <input
                type={'search'}
                onChange={(evt) => setQueryBody(evt.target.value)}
                className={`${
                  openSort1
                    ? 'absolute left-20 bottom-2 duration-500 transition-all'
                    : 'hidden'
                } outline-none border-none rounded-md bg-gray-200 py-1 px-2 text-black`}
              />
            </th>
          </tr>
        </thead>
        <tbody className="bg-white ">
          <tr className="flex flex-col items-start  ">
            {currentPost
              .filter(
                (users) =>
                  users.name.toLowerCase().includes(queryName) &&
                  users.body.toLowerCase().includes(queryBody),
              )
              .map((el, index) => (
                <tr
                  key={el.index}
                  className="flex border hover:bg-gray-100 transition-all cursor-pointer"
                >
                  <td className=" w-[60px] py-3 flex justify-center text-sm text-[#464A4D] font-medium bg-gray-100">
                    {el.id}
                  </td>
                  <td className=" w-[300px] py-3 px-2 text-sm text-[#464A4D] font-medium">
                    {el.name}
                  </td>
                  <td className=" w-[300px] py-3 px-2 text-sm text-[#464A4D] font-medium">
                    {el.email}
                  </td>
                  <td className=" w-[580px] py-3 px-2 text-sm text-[#464A4D] font-medium ">
                    {el.body}
                  </td>
                </tr>
              ))}
          </tr>
        </tbody>
      </table>

      <ul className="flex flex-wrap items-center my-4 mx-10">
        <li className='mr-4 text-blue-600 font-semibold' onClick={handlePrevBtn}>
          <button>Prev</button>
        </li>
        {pageNumbers.map((number) => {
          if (number < maxPageNumberLimit + 1 && number > minPageNumberLimit) {
            return (
              <li
                className={`${
                  currentPage == number ? 'bg-blue-600 text-white' : null
                } py-2 border text-blue-600  hover:bg-blue-600 hover:text-white font-medium  my-1`}
                key={number.index}
              >
                <a onClick={() => paginate(number)} className="px-4 " href="!#">
                  {number}
                </a>
              </li>
            )
          } else {
            return null
          }
        })}

        <li className='ml-4'>
          <button className='text-blue-600 font-semibold'onClick={handleNextBtn} >Next</button>
        </li>
      </ul>
    </div>
  )
}

export default TableF
