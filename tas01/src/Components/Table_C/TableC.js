import React, { Component } from 'react'
import axios from 'axios'
import sort from '../../Assets/images/sort.svg'

class TableC extends Component {
  state = {
    comments: [],
    loading: false,
    currentPage: 1,
    limit: 10,
    openSort: false,
    openSort1: false,
    queryName: '',
    queryBody: '',
    pageNumberLimit: 10,
    maxPageNumberLimit: 10,
    minPageNumberLimit: 0,
  }

  componentDidMount() {
    this.fetchData()
  }

  fetchData = async () => {
    this.setState({ loading: true })

    const res = await axios.get(`https://jsonplaceholder.typicode.com/comments`)
    this.setState({ comments: res.data, loading: false })
  }

  IndexLastPost = () => this.state.currentPage * this.state.limit
  IndexFirstPost = () => this.IndexLastPost - this.state.limit
  currentPost = () =>
    this.state.comments.slice(this.IndexFirstPost, this.IndexLastPost)

  paginate = (pageNumbers) => this.setState({ currentPage: pageNumbers })

  handleNextBtn = () => {
    this.setState({ currentPage: this.state.currentPage + 1 })

    if (this.state.currentPage + 1 > this.state.maxPageNumberLimit) {
      this.setState({
        maxPageNumberLimit:
          this.state.maxPageNumberLimit + this.state.pageNumberLimit,
        minPageNumberLimit:
          this.state.minPageNumberLimit + this.state.pageNumberLimit,
      })
    }
  }

  handlePrevBtn = () => {
    this.setState({ currentPage: this.state.currentPage - 1 })

    if ((this.state.currentPage - 1) % this.state.pageNumberLimit === 0) {
      this.setState({
        maxPageNumberLimit:
          this.state.maxPageNumberLimit - this.state.pageNumberLimit,
        minPageNumberLimit:
          this.state.minPageNumberLimit - this.state.pageNumberLimit,
      })
    }
  }

  render() {
    return (
      <div className="  flex flex-col justify-center">
        <table className="w-[800px] ">
          <thead className=" bg-gray-900 ">
            <tr className="flex  rounded-t-3xl ">
              <th className=" flex justify-center min-w-[60px] py-3  text-md text-[#d6d6d6] font-medium">
                ID
              </th>
              <th className=" relative flex items-center min-w-[300px] py-3 px-2 text-md text-[#d6d6d6] font-medium">
                Name
                <img
                  src={sort}
                  alt="photo"
                  className="ml-2 mt-1 cursor-pointer animate-pulse"
                  width={20}
                  onClick={() => this.setOpenSort(!this.openSort)}
                />
                <input
                  type={'search'}
                  onChange={(evt) => this.setQueryName(evt.target.value)}
                  className={`${
                    this.openSort
                      ? 'absolute left-20 duration-500 transition-all'
                      : 'hidden'
                  } outline-none border-none rounded-md bg-gray-200 py-1 px-2 text-black`}
                />
              </th>
              <th className=" flex items-start min-w-[300px] py-3 px-2 text-md text-[#d6d6d6] font-medium">
                Email
              </th>
              <th className="relative flex items-start min-w-[580px] py-3 px-2 text-md text-[#d6d6d6] font-medium">
                Body
                <img
                  src={sort}
                  alt="photo"
                  className="ml-2 mt-1 cursor-pointer animate-pulse"
                  width={20}
                  onClick={() => this.setOpenSort1(!this.openSort1)}
                />
                <input
                  type={'search'}
                  onChange={(evt) => this.setQueryBody(evt.target.value)}
                  className={`${
                    this.openSort1
                      ? 'absolute left-20 bottom-2 duration-500 transition-all'
                      : 'hidden'
                  } outline-none border-none rounded-md bg-gray-200 py-1 px-2 text-black`}
                />
              </th>
            </tr>
          </thead>
          <tbody className="bg-white ">
            <tr className="flex flex-col items-start  ">
              {this.currentPost
                .filter(
                  (users) =>
                    users.name.toLowerCase().includes(this.queryName) &&
                    users.body.toLowerCase().includes(this.queryBody),
                )
                .map((el, index) => (
                  <tr
                    key={el.index}
                    className="flex border hover:bg-gray-100 transition-all cursor-pointer"
                  >
                    <td className=" w-[60px] py-3 flex justify-center text-sm text-[#464A4D] font-medium bg-gray-100">
                      {el.id}
                    </td>
                    <td className=" w-[300px] py-3 px-2 text-sm text-[#464A4D] font-medium">
                      {el.name}
                    </td>
                    <td className=" w-[300px] py-3 px-2 text-sm text-[#464A4D] font-medium">
                      {el.email}
                    </td>
                    <td className=" w-[580px] py-3 px-2 text-sm text-[#464A4D] font-medium ">
                      {el.body}
                    </td>
                  </tr>
                ))}
            </tr>
          </tbody>
        </table>

        <ul className="flex flex-wrap items-center my-4 mx-10">
          <li
            className="mr-4 text-blue-600 font-semibold"
            onClick={this.handlePrevBtn}
          >
            <button>Prev</button>
          </li>
          {this.pageNumbers.map((number) => {
            if (
              number < this.maxPageNumberLimit + 1 &&
              number > this.minPageNumberLimit
            ) {
              return (
                <li
                  className={`${
                    this.currentPage == number ? 'bg-blue-600 text-white' : null
                  } py-2 border text-blue-600  hover:bg-blue-600 hover:text-white font-medium  my-1`}
                  key={number.index}
                >
                  <a
                    onClick={() => this.paginate(number)}
                    className="px-4 "
                    href="!#"
                  >
                    {number}
                  </a>
                </li>
              )
            } else {
              return null
            }
          })}

          <li className="ml-4">
            <button
              className="text-blue-600 font-semibold"
              onClick={this.handleNextBtn}
            >
              Next
            </button>
          </li>
        </ul>
      </div>
    )
  }
}

export default TableC
